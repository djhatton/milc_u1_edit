/* ************************************************************	*/
/*								*/
/* 			       MOMGAUGE.C	   		*/
/*								*/
/* Generate U(1) fields A_\mu(p) in momentum space: 		*/
/*  S= 1/2 \sum(k) [A0(k)^2 \vec{k'}^2 + \vec{A}^2 k'^2]	*/
/*     k'^2 = \sum [4 (sin k/2)^2] 				*/
/*  \eta(k)_\mu = gaussrand()					*/
/*  A0(k) = [\vec{k'}^2/2]^{-1/2} * \eta{k}			*/
/*  \vec{A}(k) = [k'^2/2]^{-1/2} * \eta{k}			*/
/*								*/
/* 6/5/12 CD Return the FT of the Re A(x) only                  */
/*								*/
/*								*/
/* ************************************************************	*/

#include "include_u1g.h"

void momgauge(complex *u1gf)
{

  complex Am,Ap;
  Real lk[4],mm,mp,r1,r2,newlk[4];
  Real lkssq,lktsq,lksq,newlksq;
  register site *s;
  register int i,dir;
  int hp;

  /* initialize */
  FORALLSITES(i,s){
      FORALLUPDIR(dir){
	  u1gf[4*i+dir]=cmplx(0.0,0.0);
	 }
    }

 
  FORALLSITES(i,s){
    Real mom[4] = { 2.*PI*s->x/nx, 2.*PI*s->y/ny, 2.*PI*s->z/nz, 2.*PI*s->t/nt };
    
    if(latin[i] == i)hp = 1;
    else hp = 2;


      /* lattice mom: \vec{k}^2, k1,k2,k3 and squares */
      lkssq=0.0;
      for(dir=XUP;dir<=ZUP;dir++){
          lk[dir]=2.0*(Real)sin((double)(mom[dir])/2.0);
	  lkssq+=sqr(lk[dir]);
	 }
      if(lkssq==0.0)				/* \vec{k}==0 */
	{
	  u1gf[4*i+TUP]=cmplx(0.0,0.0);
		u1gf[4*i+XUP]=cmplx(0.0,0.0);
		u1gf[4*i+YUP]=cmplx(0.0,0.0);
		u1gf[4*i+ZUP]=cmplx(0.0,0.0);
	}

	else if(mom[0]==PI && mom[1]==PI && mom[2]==PI && mom[3]==PI)
      {

			lkssq=lktsq=lksq=0.0;
      for(dir=XUP;dir<=TUP;dir++){
    lk[dir]=2.0*(Real)sin((double)(mom[dir])/2.0);
    if(dir!=TUP) lkssq+=sqr(lk[dir]);
    if(dir==TUP) lktsq+=sqr(lk[dir]);
   }
    lksq=lkssq+lktsq;
			
        u1gf[4*i+TUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
          u1gf[4*i+XUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
          u1gf[4*i+YUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
          u1gf[4*i+ZUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
      }
 
      else	
	{


    if(latin[i] == i)hp = 1;
    else hp = 2;
      /* lattice mom: k^2, k0,k1,k2,k3 and squares */
      lkssq=lktsq=lksq=0.0;
      for(dir=XUP;dir<=TUP;dir++){
	  lk[dir]=2.0*(Real)sin((double)(mom[dir])/2.0);
	  if(dir!=TUP) lkssq+=sqr(lk[dir]);
	  if(dir==TUP) lktsq+=sqr(lk[dir]);
	 }
      lksq=lkssq+lktsq;


          u1gf[4*i+TUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+TUP].imag=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+XUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+XUP].imag=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+YUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+YUP].imag=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+ZUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));
          u1gf[4*i+ZUP].imag=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(1.0/lksq));



   }

		if(mom[0]==PI || mom[0]==0)
      {
        if(mom[1]==PI || mom[1]==0)
        {
          if(mom[2]==PI || mom[2]==0)
          {
            if(mom[3]==PI || mom[3]==0)
            {
              u1gf[4*i+TUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
              u1gf[4*i+TUP].imag=0.0;
              u1gf[4*i+XUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
              u1gf[4*i+XUP].imag=0.0;
              u1gf[4*i+YUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
              u1gf[4*i+YUP].imag=0.0;
              u1gf[4*i+ZUP].real=
                (Real)(gaussian_rand_no(&(s->site_prn))*sqrt(2.0/lksq));
              u1gf[4*i+ZUP].imag=0.0;
             }
           }
         }
      }


	  if(lkssq==0.0)        /* make sure QED_L didn't get messed up */
  {
    u1gf[4*i+TUP]=cmplx(0.0,0.0);
    u1gf[4*i+XUP]=cmplx(0.0,0.0);
    u1gf[4*i+YUP]=cmplx(0.0,0.0);
    u1gf[4*i+ZUP]=cmplx(0.0,0.0);
  } 

  } /* FORALLSITES-ends */

	/* Extra e^{iak/2} factor */

	FORALLSITES(i,s){
       Real mom[4] = { 2.*PI*s->x/nx, 2.*PI*s->y/ny, 2.*PI*s->z/nz, 2.*PI*s->t/nt };
       complex phase[4] = {ce_itheta(mom[0]/2.),ce_itheta(mom[1]/2.),ce_itheta(mom[2]/2.),ce_itheta(mom[3]/2.)};
       u1gf[4*i+TUP] = cmul(&phase[3],&u1gf[4*i+TUP]);
       u1gf[4*i+XUP] = cmul(&phase[0],&u1gf[4*i+XUP]);
       u1gf[4*i+YUP] = cmul(&phase[1],&u1gf[4*i+YUP]);
       u1gf[4*i+ZUP] = cmul(&phase[2],&u1gf[4*i+ZUP]);
     }

  /* Arrange components so that Fourier transform is real */

	FORALLSITES(i,s){
              int newx,newy,newz,newt;
              newx = ((nx)-s->x)%nx;
              newy = ((ny)-s->y)%ny;
              newz = ((nz)-s->z)%nz;
              newt = ((nt)-s->t)%nt;

              Real mom[4] = { 2.*PI*s->x/nx, 2.*PI*s->y/ny, 2.*PI*s->z/nz, 2.*PI*s->t/nt };
              Real newmom[4] = { 2.*PI*newx/nx, 2.*PI*newy/ny, 2.*PI*newz/nz, 2.*PI*newt/nt };

              if(s->x==0){newx=0;}
              if(s->x==nx/2){newx=nx/2;}
              if(s->y==0){newy=0;}
              if(s->y==ny/2){newy=ny/2;}
              if(s->z==0){newz=0;}
              if(s->z==nz/2){newz=nz/2;}
              if(s->t==0){newt=0;}
              if(s->t==nt/2){newt=nt/2;}

              lksq=0.0;
              newlksq=0.0;
              for(dir=XUP;dir<=TUP;dir++){
                lk[dir]=2.0*(Real)sin((double)(mom[dir])/2.0);
                lksq+=lk[dir]*lk[dir];
                newlk[dir]=2.0*(Real)sin((double)(newmom[dir])/2.0);
                newlksq+=newlk[dir]*newlk[dir];
              }

              u1gf[4*i+TUP].real = u1gf[4*node_index(newx,newy,newz,newt )+TUP].real;
              u1gf[4*i+TUP].imag = -u1gf[4*node_index(newx,newy,newz,newt )+TUP].imag;
              u1gf[4*i+XUP].real = u1gf[4*node_index(newx,newy,newz,newt )+XUP].real;
              u1gf[4*i+XUP].imag = -u1gf[4*node_index(newx,newy,newz,newt )+XUP].imag;
              u1gf[4*i+YUP].real = u1gf[4*node_index(newx,newy,newz,newt )+YUP].real;
              u1gf[4*i+YUP].imag = -u1gf[4*node_index(newx,newy,newz,newt )+YUP].imag;
              u1gf[4*i+ZUP].real = u1gf[4*node_index(newx,newy,newz,newt )+ZUP].real;
              u1gf[4*i+ZUP].imag = -u1gf[4*node_index(newx,newy,newz,newt )+ZUP].imag;

	}

  /* Adjust u1gf so we return FT of Re A(k) */
  //FORALLSITES(i,s){
    //if(i==latin[i]){
      //for(dir=XUP;dir<=TUP;dir++){ 
	//u1gf[4*i+dir].imag=0.0;
      //}
    //} else {
    //  for(dir=XUP;dir<=TUP;dir++){ 
	//CMULREAL(u1gf[4*i+dir],2.0,u1gf[4*i+dir]);
      //}
    //}
  //}

} /* end of momgauge() */

/* ************************************************************	*/

